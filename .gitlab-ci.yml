image: docker:20.10.14

variables:
  # When you use the dind service, you must instruct Docker to talk with
  # the daemon started inside of the service. The daemon is available
  # with a network connection instead of the default
  # /var/run/docker.sock socket. Docker 19.03 does this automatically
  # by setting the DOCKER_HOST in
  # https://github.com/docker-library/docker/blob/d45051476babc297257df490d22cbd806f1b11e4/19.03/docker-entrypoint.sh#L23-L29
  #
  # The 'docker' hostname is the alias of the service container as described at
  # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services.
  #
  # Specify to Docker where to create the certificates. Docker
  # creates them automatically on boot, and creates
  # `/certs/client` to share between the service and job
  # container, thanks to volume mount from config.toml
  DOCKER_TLS_CERTDIR: "/certs"

stages:
  - test
  - build
  - integration
  - release

# The plan would be for this cache to be reused by all jobs.
# Caches currently end up cached per runner, per job concurrency level and per md5 of path?
# So there are lots of duplicate caches that end up needing to be populated right now?
# https://forum.gitlab.com/t/confusion-around-ci-docker-cache-volumes-and-sharing-across-jobs-concurrency/56793
# Docker cache volumes look like this runner-<short-token>-project-<id>-concurrent-<concurrency-id>-cache-<md5-of-path>
cache:
  - key: mediawiki
    paths:
      - .mediawiki

services:
  - name: docker:20.10.14-dind

test:
    stage: test
    needs: []
    cache: {}
    image: golang:1.16.9
    artifacts:
      reports:
        cobertura: coverage.xml
    before_script:
      - go install github.com/bwplotka/bingo@latest
      - bingo get
    script:
      - make test
      - go get github.com/boumenot/gocover-cobertura
      - gocover-cobertura < coverage.txt > coverage.xml

checks:
    stage: test
    needs: []
    cache: {}
    image: golang:1.16.9
    parallel:
      matrix:
        - CHECK: lint
        - CHECK: vet
        - CHECK: staticcheck
        - CHECK: git-state
    before_script:
      - go install github.com/bwplotka/bingo@latest
      - bingo get
    script:
      # XDG_CACHE_HOME is needed by staticcheck
      - export XDG_CACHE_HOME=/tmp/mwcli-cache
      - make $CHECK

build:
    stage: build
    needs: []
    cache: {}
    image: golang:1.16.9
    artifacts:
      paths:
        - bin/
    before_script:
      - go install github.com/bwplotka/bingo@latest
      - bingo get
    script:
      - make build


integration-general:
    stage: integration
    needs: [checks,test,build]
    cache: {}
    dependencies:
      - build
    parallel:
      matrix:
        - TEST: test-general-commands.sh
    before_script:
      # libc6-compat needed because https://stackoverflow.com/questions/36279253/go-compiled-binary-wont-run-in-an-alpine-docker-container-on-ubuntu-host
      - apk add --no-cache libc6-compat bash
    script:
      - ./tests/$TEST

integration-docker:
    stage: integration
    needs: [checks,test,build]
    dependencies:
      - build
    parallel:
      matrix:
        - TEST: test-docker-general.sh
        - TEST: test-docker-mw-all-dbs.sh
        - TEST: test-docker-mw-mysql-cycle.sh
    before_script:
      # libc6-compat needed because https://stackoverflow.com/questions/36279253/go-compiled-binary-wont-run-in-an-alpine-docker-container-on-ubuntu-host
      - apk add --no-cache libc6-compat bash docker-compose curl tar
    script:
      - ./tests/$TEST

build-release:
    except:
      - tags
    stage: release
    needs: [build,integration-general,integration-docker]
    cache: {}
    image: golang:1.16.9
    before_script:
      - go install github.com/bwplotka/bingo@latest
      - bingo get
    script:
      - make release VERSION=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}

release-build:
    only:
      - tags
    stage: release
    needs: [build,integration-general,integration-docker]
    cache: {}
    image: golang:1.16.9
    artifacts:
      paths:
        - _release/
    before_script:
      - go install github.com/bwplotka/bingo@latest
      - bingo get
    script:
      - make release VERSION=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}

release-upload:
  only:
    - tags
  stage: release
  needs: [release-build]
  dependencies:
    - release-build
  # Has 1 requirement of curl, could be replaced by a wmf releng image?
  image: alpine:latest
  before_script:
    - apk update
    - apk add curl
  # https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-generic-package-by-using-cicd
  script:
    - echo "Placeholder job, as a conditional upload currently happens as part of release-build awaiting decision on https://phabricator.wikimedia.org/T292372"
    - >
      for release_path in  $(find ./_release -type f); do
        release_file=$(echo $release_path | sed "s/.*\///")
        curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${release_path} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/mwcli/${CI_COMMIT_TAG}/${release_file}"
      done

release-publish:
  only:
    - tags
  stage: release
  needs: [release-upload,release-build]
  dependencies:
    - release-build
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - (echo "## ${CI_COMMIT_TAG}" && echo "CHANGELOG extracted from https://gitlab.wikimedia.org/repos/releng/cli/-/blob/main/CHANGELOG.md" && sed "/^## ${CI_COMMIT_TAG}$/,/^## /"'!d'";//d;/\^$/d" CHANGELOG.md) > .gitlab.description.md
    - assets_links=""
    - >
      for release_path in  $(find ./_release -type f); do
        release_file=$(echo $release_path | sed "s/.*\///")
        assets_links="${assets_links} --assets-link {\"name\":\"${release_file}\",\"url\":\"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/mwcli/${CI_COMMIT_TAG}/${release_file}\"}"
      done
    - release-cli create --name "Release $CI_COMMIT_TAG" --description ".gitlab.description.md" --released-at "${CI_COMMIT_TIMESTAMP}" ${assets_links}
